# El penjat

El pots executar així
```shell
go run codeberg.org/guineu/pengo@latest
```

També el pots compilar
## Compilar

```shell
git clone https://codeberg.org/guineu/pengo.git
go build
./pengo
```

Si no saps que es go, segueix [aquest link](https://go.dev)