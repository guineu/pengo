package main

import (
	"fmt" // Formatar (go)

	"codeberg.org/guineu/pengo/res"   // Recursos (procesament i ASCII)
	. "github.com/logrusorgru/aurora" // Colors
	"github.com/zs5460/art"           // Banner ASCII
)

func main() {
	// Inici, banner i tal
	fmt.Print(Blue(art.String("pengo")))
	fmt.Println("fet per", Magenta("guineu"), "amb", Bold("go").Cyan().Italic())
	fmt.Println(Bold("Introduïu els volstres noms:"))
	fmt.Println(Bold("[!]").Red(), Red("Els noms no poden estar buits o repetits."))

	// Demanar noms dels jugadors
	nom, nom2 := "", ""
	for res.ValString(nom, nom2) {
		fmt.Printf("Jugador 1: ")
		fmt.Scanln(&nom)
		fmt.Printf("Jugador 2: ")
		fmt.Scanln(&nom2)
	}
	fmt.Println(Bold("[✓]").Green(), Green("Jugadors registrats. "))

	// Assignem als jugadors els seus rols
	preguntador := nom
	preguntat := nom2

	// Cridem a la funció principal per primer cop
	guanyador := joc(res.Art, preguntat, preguntador)
	// POST PARTIDA
	punts, punts2 := 0, 0
	for {
		if guanyador == nom {
			punts++
		} else {
			punts2++
		}

		// Ho mostrem
		fmt.Println(Green("------------------ Partida Acabada ------------------").Bold())
		fmt.Println(Green("El jugador/a"), Bold(nom).Green(), Green("te"), Bold(punts), Green("punts i el jugador/a"), Bold(nom2).Green(), "en te", Bold(punts2))
		fmt.Println(Blue("Prem ENTER per tornar a jugar o cualsevol atra cosa per sortir"))
		opt := ""
		fmt.Scanln(&opt)
		if opt == "" {
			if guanyador == nom {
				preguntador = nom2
				preguntat = nom
			} else if guanyador == nom2 {
				preguntador = nom
				preguntat = nom2
			}
			fmt.Println("Els rols han canviat!! Ara", preguntat, "es el que juga i", preguntador, "el que pregunta!")
			guanyador = joc(res.Art, preguntat, preguntador)
		} else {
			break
		}

	}

	res.Netejar()
	fmt.Println(nom, "ha aconseguit", punts, "punts i", nom2, "ha aconseguit", punts2)
	fmt.Println("Gracies per jugar!!")
}

func joc(art []string, preguntat string, preguntador string) string {
	guanyador, paraula, pista := "", "", "" // Inicialitzem variables
	var lletres []string
	for {
		fmt.Println(Blue("[?]").Bold(), Blue(preguntador), Blue("introdueix la paraula"))
		fmt.Scanln(&paraula)
		if !res.ValString(paraula, "paraula") {
			break
		}
	}

	for {
		fmt.Println(Blue("[?]").Bold(), Blue(preguntador), Blue("introdueix la pista"))
		fmt.Scanln(&pista)
		if !res.ValString(pista, "pista") {
			break
		}
	}
	lletres = res.ProcParaula(paraula)
	estatPenjat := -1
	endevinat := []string{"init"}
	lletra := ""
	for res.PartidaAcabada(estatPenjat, endevinat, preguntador, preguntat)[1] == "false" {
		res.Netejar()
		fmt.Println(Yellow("-------- Es torn de").Bold(), Yellow(preguntat).Bold(), Yellow("--------").Bold())
		endevinat = res.Endevinar(lletres, lletra, endevinat)
		if endevinat[0] == "error" {
			estatPenjat++
		}
		if estatPenjat > 6 {
			break
		}
		fmt.Printf("\n%v\n", Blue(res.Art[estatPenjat]))
		fmt.Println(Bold("La pista es:"), pista)
		fmt.Scanln(&lletra)
	}
	guanyador = res.PartidaAcabada(estatPenjat, endevinat, preguntador, preguntat)[0]
	return guanyador
}
