package res

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
)

// FUNCIÓ PER VALIDAR STRINGS
// Rep una string i una clau. La clau s'utilitza per determinar si es una paraula
// o una pista
func ValString(paraula string, clau string) bool {
	if paraula == "" && (clau == "pista" || clau == "paraula") || len(paraula) > 10 {
		fmt.Printf("No has intoduït una %v o es massa llarga\n", clau)
		return true
	}
	// Utilitzo la clau com a segona paraula per validar el nom
	if paraula == "" || clau == "" || paraula == clau {
		return true
	} else {
		return false
	}
}

// Dividir la paraula en lletres minuscules
func ProcParaula(paraula string) []string {
	paraula = strings.ToLower(paraula)                // Minuscules
	var lletres []string = strings.Split(paraula, "") // Gràcies go
	return lletres
}

func Endevinar(lletres []string, lletrain string, endevinat []string) []string {
	lletrain = strings.ToLower(lletrain) // Minuscules per consistencia
	// Endevinat coincideixi amb el numero de lletres la primera vegada que es crida
	if endevinat[0] == "init" {
		endevinat[0] = "be"
		for i := 0; i < len(lletres); i++ {
			endevinat = append(endevinat, "_")
		}
	}

	endevinat[0] = "error"

	// Comprobar si es una lletra i afegirla
	for l := 1; l < len(endevinat); l++ {
		if lletrain == lletres[l-1] {
			endevinat[l] = lletrain
			endevinat[0] = "be"
		}
	}

	// Printar la cadena
	fmt.Printf("Paraula: ")
	for i := 1; i < len(endevinat); i++ {
		fmt.Print(endevinat[i])
	}
	return endevinat
}

func PartidaAcabada(estatPenjat int, endevinat []string, preguntador string, preguntat string) [2]string {
	guanyador := [2]string{"", "false"}
	if estatPenjat == 6 {
		guanyador[0] = preguntador
		guanyador[1] = "true"
	}
	contador := 0
	for _, v := range endevinat {
		if v != "_" {
			contador++
		}
	}
	if contador == len(endevinat)-1 {
		guanyador[0] = preguntat
		guanyador[1] = "true"
	}
	return guanyador
}

func Netejar() {
	cmd := exec.Command("clear")
	cmd.Stdout = os.Stdout
	cmd.Run()
}
