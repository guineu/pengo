module codeberg.org/guineu/pengo

go 1.18

require (
	github.com/logrusorgru/aurora v2.0.3+incompatible
	github.com/zs5460/art v0.2.0
)
